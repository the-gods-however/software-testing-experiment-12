
/**
 * 工具类
 * 计算nextday问题
 */
@SuppressWarnings("checkstyle:JavadocType")
final class Util {
    /**
     * 输入错误返回字符串
     */
    private static final String ERROR = "输入错误！";
    /**
     * 1900年的年份常数
     */
    private static final int YEAR_1900 = 1900;
    /**
     * 2050年的年份常数
     */
    private static final int YEAR_2050 = 2050;
    /**
     * 400的年份计算常数
     */
    private static final int YEAR_400 = 400;
    /**
     * 4的年份计算常数
     */
    private static final int YEAR_4 = 4;
    /**
     * 100的年份计算常数
     */
    private static final int YEAR_100 = 100;
    /**
     * 1月的月份常数
     */
    private static final int MONTH_01 = 1;
    /**
     * 2月的月份常数
     */
    private static final int MONTH_02 = 2;
    /**
     * 3月的月份常数
     */
    private static final int MONTH_03 = 3;
    /**
     * 4月的月份常数
     */
    private static final int MONTH_04 = 4;
    /**
     * 5月的月份常数
     */
    private static final int MONTH_05 = 5;
    /**
     * 6月的月份常数
     */
    private static final int MONTH_06 = 6;
    /**
     * 7月的月份常数
     */
    private static final int MONTH_07 = 7;
    /**
     * 8月的月份常数
     */
    private static final int MONTH_08 = 8;
    /**
     * 9月的月份常数
     */
    private static final int MONTH_09 = 9;
    /**
     * 10月的月份常数
     */
    private static final int MONTH_10 = 10;
    /**
     * 11月的月份常数
     */
    private static final int MONTH_11 = 11;
    /**
     * 12月的月份常数
     */
    private static final int MONTH_12 = 12;
    /**
     * 0的日期常数
     */
    private static final int DAY_0 = 0;
    /**
     * 28的日期常数
     */
    private static final int DAY_28 = 28;
    /**
     * 29的日期常数
     */
    private static final int DAY_29 = 29;
    /**
     * 30的日期常数
     */
    private static final int DAY_30 = 30;
    /**
     * 31的日期常数
     */
    private static final int DAY_31 = 31;

    private Util() {
    }

    /**
     * nextday工具方法
     *
     * @param y 指代年份
     * @param m 指代月份
     * @param d 指代日期
     * @return 返回下一天
     */
    static String nextDay(int y, int m, int d) {
        if (y >= YEAR_1900 && y <= YEAR_2050) {
            switch (m) {
                case MONTH_01:
                case MONTH_03:
                case MONTH_05:
                case MONTH_07:
                case MONTH_08:
                case MONTH_10:
                    if (d > DAY_0 && d < DAY_31) {
                        return y + "-" + m + "-" + (d + 1);
                    } else if (d == DAY_31) {
                        return y + "-" + (m + 1) + "-" + "1";
                    } else {
                        return ERROR;
                    }
                case MONTH_04:
                case MONTH_06:
                case MONTH_09:
                case MONTH_11:
                    if (d > DAY_0 && d < DAY_30) {
                        return y + "-" + m + "-" + (d + 1);
                    } else if (d == DAY_30) {
                        return y + "-" + (m + 1) + "-" + "1";
                    } else {
                        return ERROR;
                    }
                case MONTH_12:
                    if (d > DAY_0 && d < DAY_31) {
                        return y + "-" + m + "-" + (d + 1);
                    } else if (d == DAY_31) {
                        return (y + 1) + "-" + "1" + "-" + "1";
                    } else {
                        return ERROR;
                    }
                case MONTH_02:
                    if ((y % YEAR_400 == 0) || (y % YEAR_4 == 0 && y % YEAR_100 != 0)) {
                        if (d > DAY_0 && d < DAY_29) {
                            return y + "-" + m + "-" + (d + 1);
                        } else if (d == DAY_29) {
                            return y + "-" + (m + 1) + "-" + "1";
                        } else {
                            return ERROR;
                        }
                    } else {
                        if (d > DAY_0 && d < DAY_28) {
                            return y + "-" + m + "-" + (d + 1);
                        } else if (d == DAY_28) {
                            return y + "-" + (m + 1) + "-" + "1";
                        } else {
                            return ERROR;
                        }
                    }
                default:
                    return ERROR;
            }
        } else {
            return ERROR;
        }
    }
}
