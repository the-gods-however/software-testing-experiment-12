import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class TestNextdate {
    private int year, month, day;
    private String expected;

    public TestNextdate(int year, int month, int day, String expected) {
        this.year = year;
        this.month = month;
        this.day = day;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection nextParas() {
        return Arrays.asList(new Object[][]{
                //行覆盖与分支覆盖
                {2051, 13, 32, "输入错误！"},
                {2020, 2, 29, "2020-3-1"},
                {2021, 2, 28, "2021-3-1"},
                {2022, 12, 31, "2023-1-1"},
                {2022, 5, 31, "2022-6-1"},
                {2022, 4, 30, "2022-5-1"},
                {2022, 8, 15, "2022-8-16"},
                {2004, 2, 30, "输入错误！"},
                //条件覆盖
                {2051, 13, 32, "输入错误！"},
                {1899, 0, 0, "输入错误！"},
                {2020, 2, 29, "2020-3-1"},
                {2021, 2, 29, "输入错误！"},
                {2021, 2, 28, "2021-3-1"},
                {2020, 2, 28, "2020-2-29"},
                {2022, 12, 31, "2023-1-1"},
                {2020, 12, 31, "2021-1-1"},
                {2022, 5, 31, "2022-6-1"},
                {2022, 4, 31, "输入错误！"},
                {2022, 4, 30, "2022-5-1"},
                {2022, 5, 30, "2022-5-31"},
                {2022, 8, 15, "2022-8-16"},
                {2004, 2, 30, "输入错误！"},
        });
    }

    @Test
    public void testTri() {
        Assert.assertEquals(expected, Util.nextDay(year, month, day));
    }

}
